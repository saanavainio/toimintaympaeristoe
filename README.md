# Toimintaympäristö

SQLite:llä luotu tietokanta, joka kuvaa yliopiston kursseja, ilmoittautumisia ja opiskelijoita.

Code-kansiosta löytyy komennot, jotka syötetään SQLiteen, joilla luodaan tietokanta. Documents-kansiosta löytyy projektin dokumentaatio.
