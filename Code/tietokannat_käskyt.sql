CREATE TABLE Kurssi (
Koodi TEXT PRIMARY KEY,
Nimi TEXT NOT NULL,
Opintopisteet INTEGER NOT NULL
);

CREATE TABLE Aika (
Päivämäärä TEXT, 
Kellonaika TEXT,
PRIMARY KEY (Päivämäärä, Kellonaika)
);

CREATE TABLE Harjoitusryhmä (
RyhmäID TEXT PRIMARY KEY, 
Maksimimäärä INTEGER,
KertaID TEXT REFERENCES Kurssikerta(KertaID), 
Päivämäärä TEXT,
AlkamisAika TEXT,
Kesto INTEGER,
FOREIGN KEY (Päivämäärä, Alkamisaika) REFERENCES Aika (Päivämäärä, Kellonaika)
);

CREATE TABLE Tentti (
TenttiID TEXT PRIMARY KEY, 
KurssiKoodi TEXT REFERENCES Kurssi(Koodi),
Päivämäärä TEXT,
AlkamisAika TEXT,
Kesto INTEGER,
FOREIGN KEY (Päivämäärä, Alkamisaika) REFERENCES Aika (Päivämäärä, Kellonaika)
);

CREATE TABLE Salivaraus (
Varaustunnus TEXT PRIMARY KEY,
VarausPäivä TEXT NOT NULL,
VarausAlkaa TEXT NOT NULL,
VarausLoppuu TEXT NOT NULL,
TyöntekijäTunnus TEXT REFERENCES Työntekijä (Tunnus), 
SaliID TEXT REFERENCES Sali (SaliID), 
RyhmäID TEXT REFERENCES HarjoitusRyhmä (RyhmäID), 
LuentoID TEXT REFERENCES Luento (LuentoID),
TenttiID TEXT REFERENCES Tentti (TenttiID)
);

CREATE TABLE Luento (
LuentoID TEXT PRIMARY KEY,
KertaID TEXT REFERENCES Kurssikerta(KertaID),
Päivämäärä TEXT,
AlkamisAika TEXT,
Kesto INTEGER,
FOREIGN KEY (Päivämäärä, Alkamisaika) REFERENCES Aika(Päivämäärä, Kellonaika)
);

CREATE TABLE Kurssikerta (
KertaID TEXT PRIMARY KEY,
KurssiKoodi TEXT REFERENCES Kurssi(Koodi)
);

CREATE TABLE Ajankohta (
Vuosi INTEGER NOT NULL,
Periodi INTEGER CHECK (Periodi IN (1,2,3,4,5)),
PRIMARY KEY (Vuosi, Periodi)
);

CREATE TABLE Opiskelija (
OpNro TEXT PRIMARY KEY,
Nimi TEXT NOT NULL,
SyntymäAika TEXT,
Ohjelma TEXT,
Aloitus INTEGER,
Lopetus INTEGER
);

CREATE TABLE Yliopisto (
Nimi TEXT PRIMARY KEY
);

CREATE TABLE Rakennus (
Nimi TEXT PRIMARY KEY,
Osoite TEXT NOT NULL,
YliopistoNimi TEXT REFERENCES Yliopisto(Nimi)
);

CREATE TABLE Työntekijä (
Tunnus TEXT PRIMARY KEY,
TehtäväNimike TEXT,
Nimi TEXT NOT NULL,
Osoite TEXT NOT NULL,
PuhelinNro TEXT,
AlkuPv TEXT,
Loppupv TEXT
);

CREATE TABLE Sali (
SaliID TEXT PRIMARY KEY,
Paikat INTEGER NOT NULL,
PaikatTentti INTEGER NOT NULL,
RakennusNimi TEXT REFERENCES Rakennus(Nimi)
);

CREATE TABLE Varuste(
VarusteID TEXT PRIMARY KEY,
SaliID TEXT REFERENCES Sali(SaliID)
);

CREATE TABLE Tenttiilmo (
OpNro TEXT,
TenttiID TEXT, 
Päivä TEXT, 
Kieli TEXT,
FOREIGN KEY (OpNro) REFERENCES Opiskelija(OpNro)
FOREIGN KEY (TenttiID) REFERENCES Tentti(TenttiID)
PRIMARY KEY (OpNro, TenttiID)
);

CREATE TABLE TenttiSuoritus (
OpNro TEXT,
TenttiID TEXT,
Arvosana INTEGER CHECK (Arvosana IN (0,1,2,3,4,5)),
FOREIGN KEY (OpNro) REFERENCES Opiskelija(OpNro)
FOREIGN KEY (TenttiID) REFERENCES Tentti(TenttiID)
PRIMARY KEY (OpNro, TenttiID)
);

CREATE TABLE Pisteet (
OpNro TEXT,
KertaID TEXT,
Määrä INTEGER NOT NULL,
FOREIGN KEY (OpNro) REFERENCES Opiskelija(OpNro)
FOREIGN KEY (KertaID) REFERENCES Kurssikerta(KertaID)
PRIMARY KEY (OpNro, KertaID, Määrä)
);

CREATE TABLE HarjoitusOsallistuminen (
RyhmäID TEXT,
OpNro TEXT,
FOREIGN KEY (RyhmäID) REFERENCES HarjoitusRyhmä(RyhmäID)
FOREIGN KEY (OpNro) REFERENCES Opiskelija(OpNro)
PRIMARY KEY (RyhmäID, OpNro)
);

CREATE TABLE KurssiAika (
KertaID TEXT,
Vuosi INTEGER,
Periodi INTEGER,
FOREIGN KEY (KertaID) REFERENCES Kurssikerta(KertaID)
FOREIGN KEY (Vuosi, Periodi) REFERENCES Ajankohta(Vuosi, Periodi)
PRIMARY KEY (KertaID, Vuosi, Periodi)
);



CREATE INDEX OpiskelijanimetHakemisto ON Opiskelija(Nimi);
CREATE INDEX KurssiNimetHakemisto ON Opiskelija(Nimi);
CREATE INDEX SalivarausAjatHakemisto ON Salivaraus(VarausAika);
CREATE INDEX TyöntekijänimetHakemisto ON Työntekijä(Nimi);
CREATE INDEX VarausHakemisto ON Salivaraus(TyöntekijäTunnus);


CREATE VIEW OpiskelijatRyhmissä AS
SELECT Opiskelija.OpNro, Nimi, Ohjelma, Harjoitusryhmä.RyhmäID, Maksimimäärä
FROM HarjoitusOsallistuminen, Opiskelija, HarjoitusRyhmä
WHERE HarjoitusOsallistuminen.RyhmäID = HarjoitusRyhmä.RyhmäID AND HarjoitusOsallistuminen.OpNro = Opiskelija.OpNro;


INSERT INTO Kurssi
VALUES ("CS-A1150", "Tietokannat", 5);

INSERT INTO Kurssi
VALUES ("NBE-C2101", "Biofysiikka", 5);

INSERT INTO Kurssi
VALUES ("ELEC-C3220", "Kvantti-ilmiöt", 5);

INSERT INTO Kurssikerta
VALUES ("Biofysiikka5", "NBE-C2101")

INSERT INTO Kurssikerta
VALUES ("Tietokannat3", "CS-A1150");

INSERT INTO Kurssikerta
VALUES ("Kvantti 3", "ELEC-C3220");

INSERT INTO Opiskelija
VALUES ("887766", "Saana Vainio", "2001-04-08", "BioIT-Maestro", 2020, 2025);

INSERT INTO Opiskelija
VALUES ("111222", "Katri Padel-Setälä", "1998-11-11", "Padel-Maestro", 2020, 2025);

INSERT INTO Opiskelija
VALUES ("333888", "Lionel Stressi", "1668-04-08", "Fusse-Maestro", 2018, 2025);

INSERT INTO Yliopisto
VALUES ("Aalto-yliopisto");

INSERT INTO Rakennus 
VALUES ("TUAS", "Servin Maijan tie 60", "Aalto-yliopisto");

INSERT INTO Sali
VALUES ("AS22", 300, 150, "TUAS");

INSERT INTO Sali
VALUES ("TU4", 30, 15, "TUAS");

INSERT INTO Aika
VALUES ("2023-05-07", "15.00:00.0");

INSERT INTO Aika
VALUES ("2020-01-10", "09.30:00.0");

INSERT INTO Aika
VALUES ("2020-03-10", "09.30:00.0");

INSERT INTO Työntekijä 
VALUES ("987654", "Rehtori", "Tarja Halonen", "Halosentie 2000", "1234567896", "2000-03-01", "2012-03-01");

INSERT INTO Työntekijä 
VALUES ("000123", "Lehtori", "Ratti Maasakka", "Jämeräntaival 20", "040 040 0404", "2010-01-01", NULL);

INSERT INTO Luento
VALUES ("Kvantti-L1", "Kvantti 3", "2020-01-10", "09.30:00.0", 120);

INSERT INTO Ajankohta
VALUES (2020, 3);

INSERT INTO Luento
VALUES ("Biofysiikka-L1", "Biofysiikka5", "2020-05-07", "15.00:00.0", 90);

INSERT INTO KurssiAika
VALUES ("Kvantti 3", 2020, 3);

INSERT INTO Varuste
VALUES ("Tietokone1", “AS22”);

INSERT INTO Tentti
VALUES ("Kvantt-tentti1", "ELEC-C3220", "2020-03-10", "09.30:00.0", 180);

INSERT INTO TenttiIlmo
VALUES ("333888", "Kvantt-tentti1", "2020-03-10", "suomi");

INSERT INTO TenttiSuoritus 
VALUES ("333888", "Kvantt-tentti1", 5);

INSERT INTO Ajankohta
VALUES (2023, 5);

INSERT INTO KurssiAika
VALUES ("Biofysiikka5", 2023, 5);

INSERT INTO Ajankohta
VALUES (2023, 3);

INSERT INTO KurssiAika
VALUES ("Tietokannat3", 2023, 3)

INSERT INTO Pisteet
VALUES ("333888", "Kvantti 3", 99)

INSERT INTO Aika
VALUES ("2020-01-11", "12.00:00.0");

INSERT INTO Harjoitusryhmä
VALUES ("Kvantti-H1", 30, "Kvantti 3", "2020-01-11", "12.00:00.0", 120);

INSERT INTO HarjoitusOsallistuminen
VALUES ("Kvantti-H1", "333888");

INSERT INTO Aika
VALUES ("2022-05-23", "10.00:00.0");

INSERT INTO Aika
VALUES ("2021-05-22", "10.00:00.0");

INSERT INTO Tentti
VALUES ("Biofysikka1", "NBE-C2101", "2022-05-23", "10.00:00.0", 180);

INSERT INTO Tentti
VALUES ("Biofysikka_vanha", "NBE-C2101", "2021-05-22", "10.00:00.0", 180);

INSERT INTO TenttiIlmo
VALUES ("887766", "Biofysikka_vanha", "2021-05-22", "suomi");

INSERT INTO TenttiSuoritus 
VALUES ("887766","Biofysikka_vanha", 1);

INSERT INTO TenttiIlmo
VALUES ("887766", "Biofysikka1", "2022-05-23", "suomi");

INSERT INTO TenttiIlmo
VALUES ("111222", "Biofysikka1", "2022-05-23", "suomi");

INSERT INTO Aika
VALUES ("2020-01-17", "09.30:00.0");

INSERT INTO Luento
VALUES ("Kvantti-L2", "Kvantti 3", "2020-01-17", "09.30:00.0", 120);

INSERT INTO Aika
VALUES ("2020-01-24", "09.30:00.0");

INSERT INTO Luento
VALUES ("Kvantti-L3", "Kvantti 3", "2020-01-24", "09.30:00.0", 120);

INSERT INTO Salivaraus
VALUES ("AS221", "2020-01-10", "09.00:00.0", "12.00:00.0", "987654", "AS22", null, "Kvantti-L1", null);

INSERT INTO Salivaraus
VALUES ("TU41", "2022-05-23", "10.00:00.0", "12.00:00.0", "000123", "TU4", NULL, NULL, "Biofysikka1");




SELECT OpNro
FROM HarjoitusOsallistuminen
LEFT JOIN Harjoitusryhmä ON Harjoitusryhmä.RyhmäID = HarjoitusOsallistuminen.RyhmäID
WHERE Harjoitusryhmä.KertaID = "Kvantti 3";

SELECT nimi, OpNro
FROM Tenttisuoritus 
LEFT JOIN Opiskelija ON Opiskelija.OpNro = Tenttisuoritus.OpNro 
WHERE Arvosana != 0 AND TenttiID = "Kvantt-tentti1";

SELECT Kurssi.Koodi, Kurssi.Nimi
FROM Kurssiaika, Kurssikerta, Kurssi
WHERE Kurssiaika.KertaID = Kurssikerta.KertaID AND Kurssikerta.KurssiKoodi = Kurssi.Koodi AND Kurssiaika.Vuosi = 2023 AND Kurssiaika.Periodi = 5;

SELECT OpNro, Nimi
FROM Opiskelija
WHERE Päivämäärä >= 2020;

SELECT TenttiID
FROM Tentti, Kurssi
WHERE Kurssi.Koodi = "ELEC-C3220" AND Kurssi.Koodi = Tentti.Kurssikoodi AND Tentti.Päivämäärä >= "2020-01-01" AND Tentti.Päivämäärä <= "2020-06-01";

SELECT Arvosana
FROM TenttiSuoritus
WHERE TenttiSuoritus.Opnro = "887766" AND TenttiID In (
	SELECT TenttiID;
    FROM Tentti, Kurssi
	WHERE Kurssi.Koodi = "NBE-C2101" AND Tentti.KurssiKoodi = Kurssi.Koodi
);

INSERT INTO TenttiIlmo
VALUES ("887766", "Biofysikka1", "2022-05-23", "suomi");


SELECT RyhmäID 
FROM HarjoitusOsallistuminen, Kurssikerta, HarjoitusRyhmä
WHERE Kurssikerta.KertaID = "Kvantti 3" AND Harjoitusryhmä.KertaID = Kurssikerta.KertaID AND HarjoitusOsallistuminen.RyhmäID = Harjoitusryhmä.RyhmäID
GROUP BY HarjoitusOsallistuminen.RyhmäID
HAVING COUNT (HarjoitusOsallistuminen.OpNro) <= Harjoitusryhmä.Maksimimäärä;

SELECT LuentoID, Päivämäärä, Alkamisaika
FROM Luento, Kurssikerta
WHERE Kurssikerta.KertaID = "Kvantti 3" AND Luento.KertaID = Kurssikerta.KertaID;

SELECT Määrä
FROM Pisteet
WHERE OpNro = "333888";

UPDATE Pisteet
SET Määrä = 105
WHERE OpNro = "333888";

SELECT Määrä
FROM Pisteet
WHERE OpNro = "333888";

SELECT Opiskelija.Nimi, Tenttiilmo.Kieli
FROM Opiskelija, Tenttiilmo
WHERE Opiskelija.OpNro = TenttiIlmo.OpNro AND Tenttiilmo.TenttiID = "Biofysikka1";

SELECT Sali.SaliID
FROM Salivaraus, Sali
WHERE Sali.SaliID = Salivaraus.SaliID AND Sali.Paikat > 50 AND Salivaraus.SaliID NOT IN (
	SELECT SaliID
	FROM Salivaraus AS S
	WHERE S.VarausPäivä = "2022-06-05" AND S.VarausAlkaa <= "09.00:00.0" AND S.VarausLoppuu >= "09.00:00.0"
);

SELECT Työntekijä.Nimi, RyhmäID, LuentoID, TenttiID
FROM Salivaraus, Työntekijä
WHERE Työntekijä.Tunnus = Salivaraus.TyöntekijäTunnus AND Salivaraus.VarausPäivä = "2022-05-23" AND Salivaraus.VarausAlkaa <= "11.00:00.0" AND Salivaraus.VarausLoppuu >= "11.00:00.0";

SELECT Kurssi.Nimi
FROM Salivaraus, Kurssikerta, Kurssi, Luento
WHERE Kurssi.Koodi = Kurssikerta.KurssiKoodi AND Salivaraus.LuentoID = Luento.LuentoID AND Luento.KertaID = Kurssikerta.KertaID AND Salivaraus.VarausPäivä = "2020-01-10" AND Salivaraus.VarausAlkaa <= "10.00:00.0" AND Salivaraus.VarausLoppuu >= "10.00:00.0";

SELECT Varaustunnus, VarausPäivä, SaliID
FROM Salivaraus
WHERE TyöntekijäTunnus = "987654";

SELECT TenttiID
FROM Tentti, Kurssi
WHERE Kurssi.Nimi = "Biofysiikka" AND Kurssi.Koodi = Tentti.KurssiKoodi AND Tentti.Päivämäärä < "2024-01-01" AND Tentti.Päivämäärä >= "2020-01-01";